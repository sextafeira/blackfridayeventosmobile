package fridayevetosapp.blackfridayteam.com.fridayapp;

import android.app.Activity;
import android.app.DownloadManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import utils.ServidorUtils;

public class CreateUserAccountActivity extends Activity {

    EditText createUserEditUserUser;
    EditText createUserEditUserEmail;
    EditText createUserEditUserPass1;
    EditText createUserEditUserPass2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user_account);

        createUserEditUserUser = (EditText)findViewById(R.id.create_user_user);
        createUserEditUserEmail = (EditText)findViewById(R.id.create_user_email);
        createUserEditUserPass1 = (EditText)findViewById(R.id.create_user_password);
        createUserEditUserPass2 = (EditText)findViewById(R.id.create_user_password_confirm);

    }

    public void createNewAccountDone(View view) throws IOException {

        final String createUserUserName = createUserEditUserUser.getText().toString();
        final String createUserUserEmail = createUserEditUserEmail.getText().toString();
        final String createUserUserPass1 = createUserEditUserPass1.getText().toString();
        String createUserUserPass2 = createUserEditUserPass2.getText().toString();

        if (createUserUserEmail.equals("")) Toast.makeText(this,R.string.empty_email,Toast.LENGTH_SHORT).show();

        if (createUserUserName.equals("")) Toast.makeText(this,R.string.empty_user_name,Toast.LENGTH_SHORT).show();

        if (createUserUserEmail.equals("")) Toast.makeText(this,R.string.empty_password,Toast.LENGTH_SHORT).show();

        if (createUserUserEmail.equals("")) Toast.makeText(this,R.string.empty_password,Toast.LENGTH_SHORT).show();

        if (! createUserUserPass1.equals(createUserUserPass2)) Toast.makeText(this,R.string.password_mismatch,Toast.LENGTH_SHORT).show();

        if (!createUserUserEmail.equals("") && !createUserUserName.equals("") && !createUserUserPass1.equals("") && createUserUserPass1.equals(createUserUserPass2)){

            // TODO create user logic
            RequestQueue queue = Volley.newRequestQueue(this);
            String url = ServidorUtils.getUserRoute();

            final JSONObject headers = new JSONObject();
            try{
            headers.put("fullname",createUserUserName);
            headers.put("email",createUserUserEmail);
            headers.put("password",createUserUserPass1);
            }catch (JSONException je){

            }

            JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, url,headers,
                    new Response.Listener<JSONObject>()
                    {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject json = new JSONObject();
                                json.put("fullname", createUserUserName);
                                json.put("email",createUserUserEmail);
                                json.put("password",createUserUserPass1);

                            }catch (JSONException je){
                                Toast.makeText(CreateUserAccountActivity.this,""+je,Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener()
                    {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error

                            Toast.makeText(CreateUserAccountActivity.this,error.networkResponse+"",Toast.LENGTH_SHORT).show();
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams()
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("fullname", createUserUserName);
                    params.put("email", createUserUserEmail);
                    params.put("password", createUserUserPass1);

                    return params;
                }
                @Override
                public Map<String,String> getHeaders(){
                    Map<String,String> headers = new HashMap<String,String>();
                    headers.put("Content-Type", "application/json");

                    return headers;
                }

            };

            queue.add(postRequest);

            Toast.makeText(this,postRequest+"finally: ",Toast.LENGTH_SHORT).show();
        }

    }
}
