package fridayevetosapp.blackfridayteam.com.fridayapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import utils.ServidorUtils;

public class MainActivity extends AppCompatActivity {

    EditText editUserEmail;
    EditText editUserPass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editUserEmail = (EditText)findViewById(R.id.userEditLogin);
        editUserPass = (EditText)findViewById(R.id.userEditPassword);

    }
    public void toDoLogin(View view){

        String email = editUserEmail.getText().toString();
        String  password = editUserPass.getText().toString();

        if (email.equals("")) Toast.makeText(this,R.string.empty_email,Toast.LENGTH_SHORT).show();

        if (password.equals("")) Toast.makeText(this,R.string.empty_password,Toast.LENGTH_SHORT).show();

        else{

            if ( !email.equals("") && !password.equals("")){

                // TODO auth logic

                final String url = ServidorUtils.getUserLoginRoute()+email+"/"+password;
                RequestQueue queue = Volley.newRequestQueue(this);

                StringRequest stringRequest = new StringRequest(Request.Method.GET,url ,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                Toast.makeText(MainActivity.this, ""+response, Toast.LENGTH_SHORT).show();

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(MainActivity.this, error+" ", Toast.LENGTH_SHORT).show();
                    }
                });

                queue.add(stringRequest);

            }
        }
    }

    public void toCreateAccountActivity(View view){

        Intent intent = new Intent(this,CreateUserAccountActivity.class);
        startActivity(intent);
    }


}
