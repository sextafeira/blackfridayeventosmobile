package utils;

import java.util.concurrent.ThreadLocalRandom;

public class Hash {
	
	public static String getStringHash(int size){
		String hash = "";
		for(int i = 0; i < size; i++){

			int indice = ThreadLocalRandom.current().nextInt(65,90);

			hash += (char)indice;
		}
		return hash;
	}
	
	public static Integer getIntHash(int size){
		String hash = "";
		for(int i = 0; i < size; i++){

			int numero = ThreadLocalRandom.current().nextInt(0,9);

			hash += (String.valueOf(numero));
		}
		return Integer.parseInt(hash);
	}
}
