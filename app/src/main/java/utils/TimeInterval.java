package com.eventos.utils;
import java.util.*;

public class TimeInterval
{
	private Date begin;
	private Date end;
	
	public TimeInterval(Date begin,Date end){
		
		this.begin = begin;
		this.end = end;
	}
	
	public boolean isInRange(Date date){
		
		return (date.compareTo(this.begin) >= 0 && date.compareTo(this.end) <= 1);
		
	}
	
	
}
