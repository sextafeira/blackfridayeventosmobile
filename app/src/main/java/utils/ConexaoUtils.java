package utils;

/**
 * Created by Home on 20/08/2017.
 */

public class ConexaoUtils {


    public static class HTTPRequestCodes{

        public static final Integer OK = 200;

        public static final Integer SUCCESS = 201;

        public static final Integer FORBIDEN = 403;

        public static final Integer NOT_FOUND = 404;

        public static final Integer MALFORMED_REQUEST = 500;

    }

    public final static String API_BASE_URL = "https://blackfridayeventosapi.herokuapp.com/";


}
