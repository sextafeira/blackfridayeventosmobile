package model;

import java.util.ArrayList;
import java.util.List;
import enums.ActivityType;
import interfaces.Happenable;

public class EventActivity implements Happenable{
	private Long id;
	private String name;
	private String description;
	private Double price;
	private ActivityType activityType;
	
	private Event event;
	private List<EventActivity> nonParalel;
	private List<Tag> tags;
	private List<Local> local;
	//
	private EventActivity() {
		this.price = 0d;
		this.nonParalel = new ArrayList<>();
	}
	
	public EventActivity(String name, Event event) {
		this();
		this.name = name;
		this.event = event;
	}
	
	public ActivityType getActivityType() {
		return activityType;
	}

	public void setActivityType(ActivityType activityType) {
		this.activityType = activityType;
	}

	//
	@Override
	public Event getEvent() {
		return this.event;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<EventActivity> getNonParalel() {
		return nonParalel;
	}

	public void setNonParalel(List<EventActivity> nonParalel) {
		this.nonParalel = nonParalel;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public List<Local> getLocal() {
		return local;
	}

	public void setLocal(List<Local> local) {
		this.local = local;
	}
}
