package model;


import enums.RelationshipType;

public class Relationship {
	private Long id;
	private RelationshipType relationshipType;
	private Partner partner;
	private Event event;
	
	private Relationship() {
		this.relationshipType = RelationshipType.PARTNERSHIP;
	}
	
	public Relationship(Partner partner, Event event) {
		this();
		this.partner = partner;
		this.event = event;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public RelationshipType getRelationshipType() {
		return relationshipType;
	}

	public void setRelationshipType(RelationshipType relationshipType) {
		this.relationshipType = relationshipType;
	}

	public Partner getPartner() {
		return partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}
}
