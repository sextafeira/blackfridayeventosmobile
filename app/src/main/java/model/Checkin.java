package model;

import java.util.Date;

public class Checkin {

	private Long id;
	private Date datetime;
	private String description;
	private User user;
	private EventActivity eventActivity;

	public Checkin() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDatetime() {
		return datetime;
	}

	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public EventActivity getEventActivity() {
		return eventActivity;
	}

	public void setEventActivity(EventActivity eventActivity) {
		this.eventActivity = eventActivity;
	}
}
