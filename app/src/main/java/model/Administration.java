package model;

import java.util.ArrayList;
import java.util.List;


public class Administration {

	private Long id;
	private List<User> users;
	private List<Event> events;
	
	private Administration() {
		this.users = new ArrayList<>();
		this.events = new ArrayList<>();
	}
	
	public Administration(Event event) {
		this();
		this.events.add(event);
		//this.users.add(event.getCreator());
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public List<Event> getEvents() {
		return events;
	}
	
	/**
	public void setEvents(List<Event> events) {
		this.events = events;
	}
	**/
	
	public void addEvent(Event event) {
		this.events.add(event);
	}
	
	public void addUser(User user) {
		this.users.add(user);
	}
}
