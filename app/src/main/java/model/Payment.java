package model;


import java.util.Date;

import enums.PaymentStatus;

public class Payment{
	private Long id;
	private Date paymentDate;
	private PaymentStatus paymentStatus;
	
	private Registration registration;
	private User creator;
	
	private Payment() {
		this.paymentDate = new Date();
		this.paymentStatus = PaymentStatus.UNPAID;
	}
	
	public Payment(Registration registration) {
		this();
		this.registration = registration;
	}
	
	public Payment(Registration registration, User creator) {
		this(registration);
		this.creator = creator;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(PaymentStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public Registration getRegistration() {
		return registration;
	}

	public void setRegistration(Registration registration) {
		this.registration = registration;
	}

	public User getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}
}
