package com.eventos.enums;

public enum MessageType
{
	SUCCESS, ERROR, ILLEGAL_ARGUMENT, INVALID_STATE
}
