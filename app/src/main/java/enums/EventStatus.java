package enums;

public enum EventStatus {
	NEW, OPEN_REGISTRATION, ONGOING, FINISHED, CANCELED;
}
