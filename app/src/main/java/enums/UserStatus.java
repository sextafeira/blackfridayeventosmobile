package enums;

public enum UserStatus {
	UNCONFIRMED, CONFIRMED;
}
