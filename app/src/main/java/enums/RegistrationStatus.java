package enums;

public enum RegistrationStatus {
	STARTED, APROVED, DENIED;
}
