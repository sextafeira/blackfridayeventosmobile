package fridayapp.br.com.fridayeventsapp;

import android.app.Activity;
import android.app.VoiceInteractor;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.media.ImageReader;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.net.URI;

public class UserViewProfileActivity extends AppCompatActivity {

    ImageView profilePicure;
    TextView profileName;
    TextView profileEmail;
    TextView profilePass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_view_profile);

        profilePicure = (ImageView)findViewById(R.id.userProfilePicture);
        profileName = (TextView)findViewById(R.id.profileUserName);
        profileEmail = (TextView)findViewById(R.id.profileUserEmail);
        profilePass = (TextView)findViewById(R.id.profileUserPass);
        profilePicure.setImageResource(R.mipmap.profile_icon);

    }

    public void changeProfilePicture(View view) {

        // TODO implements
    }

}
