package fridayapp.br.com.fridayeventsapp;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import controller.UserController;
import model.User;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import utils.ConexaoUtils;

public class CreateAccountActivity extends AppCompatActivity{

    EditText username;
    EditText userEmail;
    EditText password;
    EditText reconfirm;
    Button confirm;
    Toast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        username = (EditText)findViewById(R.id.create_user_user);
        userEmail = (EditText)findViewById(R.id.create_user_email);
        password = (EditText)findViewById(R.id.create_user_password);
        reconfirm = (EditText)findViewById(R.id.create_user_password_confirm);

    }

    public void createNewAccountDone(View view){
        String nome = username.getText().toString();
        String pass = password.getText().toString();
        String retype = reconfirm.getText().toString();
        String email = userEmail.getText().toString();

        if (nome.equals("")){
            Toast.makeText(this,R.string.empty_user_name,Toast.LENGTH_SHORT).show();
        }
        if (email.equals("")){
            Toast.makeText(this,R.string.empty_user_email,Toast.LENGTH_SHORT).show();
        }
        if (pass.equals("") || retype.equals("")){
            Toast.makeText(this,R.string.empty_user_pass,Toast.LENGTH_SHORT).show();
        }
        if (!pass.equals("") && !retype.equals("") && !pass.equals(retype)){
            Toast.makeText(this,R.string.password_mismatch,Toast.LENGTH_SHORT).show();
        }

        if(!username.equals("") && !email.equals("") && !pass.equals("") || pass.equals(retype)){

            // TODO create new user logic

            User user = new User(email,pass);
            user.setFullname("user");

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://blackfridayeventosapi.herokuapp.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            UserController controller = retrofit.create(UserController.class);

            Call<List<Object>> resultFromCall = controller.createNewUser(user);

            try {
                if (resultFromCall.execute().code() == ConexaoUtils.HTTPRequestCodes.OK) {
                    Toast.makeText(this, R.string.user_created_successfully, Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(this,R.string.user_created_failure,Toast.LENGTH_SHORT).show();
                }
            }catch (Exception e){
                //Toast.makeText(this, "ocorreu um erro"+e, Toast.LENGTH_SHORT).show();
            }
        }

    }


}
