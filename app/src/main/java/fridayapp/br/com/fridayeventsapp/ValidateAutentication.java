package fridayapp.br.com.fridayeventsapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;

import controller.UserController;
import model.User;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import utils.ConexaoUtils;

public class ValidateAutentication extends AppCompatActivity {

    public final String API_BASE = "https://blackfridayeventosapi.herokuapp.com/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.auth_loading);

        Bundle extras = getIntent().getExtras();

        String email = extras.getString("userEmail");
        String senha = extras.getString("userPass");

        Toast.makeText(this, email+"||"+senha, Toast.LENGTH_SHORT).show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConexaoUtils.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        UserController service = retrofit.create(UserController.class);

        User user = new User(email,senha);
        Call<User> authObjectCall = service.loginResponse(user);

        try {

            Response<User> responseObject = authObjectCall.execute();
            user = responseObject.body();

            if (responseObject.code() == ConexaoUtils.HTTPRequestCodes.OK) {


                //Toast.makeText(this, "SUCESSO", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(this,MainLoggedUserActivity.class);
                Gson gson = new Gson();
                gson.toJson(user);

                intent.putExtra("user",gson.toString());
                startActivity(intent);

            } else {
                Toast.makeText(this, "ERRO: email ou senha incorretos", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this,MainActivity.class);
                startActivity(intent);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onRestart () {
        super.onRestart();
    //    Toast.makeText(this, "iniciou Restart", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);


    }
}
