package controller;

import java.util.List;

import model.Tag;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Home on 20/08/2017.
 */

public interface TagController {

    @GET("/tags")
    Call<List<Tag>> getAll ();

    @POST("users/{id}/tags")
    Call<List<Tag>> createTag (@Path("id") Integer userId, @Body Tag tag);

    @DELETE("users/{id}/tags/{tagId}")
    Call<List<Tag>> deleteById (@Path("id") Integer userId, @Path("tagId") Integer tagId);

    @POST("users/{id}/tags")
    Call<List<Tag>> updateTag (@Path("id") Integer userId, @Path("tagId") Integer tagId, @Body Tag tag);

}
