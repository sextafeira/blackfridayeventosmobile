package controller;

import java.util.List;

import model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Home on 20/08/2017.
 */

public interface UserController {


    @GET("users/")
    Call<List<User>> getAll ();

    @GET("users/{id}")
    Call<List<User>> findUserById (@Path("id") Integer id);

    @POST("users/")
    Call<List<Object>> createNewUser(@Body User newUser);

    @POST("users/login")
    Call<User> loginResponse(@Body User authUser);

    @DELETE("users/{id}")
    Call <List<Object>> deleteById(@Path("id") Integer id);

}
