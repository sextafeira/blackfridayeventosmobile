package controller;

import java.util.List;

import model.Registration;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Home on 20/08/2017.
 */

public interface RegistrationController {

    @GET("registrations/")
    Call<List<Registration>> getAllRegistrations ();

    @POST("/users/{id}/registrations")
    Call<List<Object>> createRegistration (@Path("id") Integer userId, @Body Registration registration);

    @DELETE("/users/{id}/registrations/{regId}")
    Call<List<Object>> deleteRegistration (@Path("id") Integer userId, @Path("regId") Integer registrationId);

    @POST("/users/{id}/registrations/{regId}")
    Call<List<Object>> updateRegistration (@Path("id") Integer userId, @Path("regId") Integer registrationId, @Body Registration newRegistration);

}
